# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""


import numpy as np

dictionnaire = dict()
d1 = {'spiecies' : 'aaa',
     'genre' : 'aa b',
     'autre' : 'cc'
     }
d2 = {'spiecies' : 'ba',
     'genre' : 'aa ff',
     'autre' : 'frgc'
     }
d3 = {'spiecies' : 'affaa',
     'genre' : 'aa b',
     'autre' : 'ccf'
     }
 
keys = ['spiecies', 'genre', 'autre'] 

array = [d1, d2, d3]
a = [1,2,3,4, 4, 4]

print(set(a))

#indice de Shannon
def shannon(spiecies):
    spiecies_list = set(spiecies)
    N = len(spiecies)
    
    H = 0
    
    for i in spiecies_list:
        pi = spiecies.count(i)/N
        H -= pi * np.log(pi)
    
    return H


def dic_list(dic_array, key_list, depth):
    dic_list={}
    
    for d in range(depth):
        k = key_list[d]
  
        dic_list[k] = []
        for a in dic_array:
            dic_list[k].append(a[k])
            
    return dic_list


data = dic_list(array, keys, 3)  
print(data)      

def biodiversity(dic_array, key_list, depth):
    data = dic_list(dic_array, key_list, depth)
    H = {}
    for k in key_list:
        H[k]=(shannon(data[k]) )
    
    return H

#TEST
#h = shannon(a)
#print(h)
#kk = biodiversity(array, keys, 3)
#print(kk)
    
    