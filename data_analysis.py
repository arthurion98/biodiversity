# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 21:51:51 2020

@author: arthu
"""

import identification
import shannon

def process_data(files, additional_species = ''):
    
    species_list = identification.identifyBarcodes(files)
    
    if len(additional_species) > 0:
        species_list = identification.addSpecies(species_list, additional_species)
        
    b = shannon.biodiversity(species_list, ['species','genus','family','order','class','phylum'], 6)
    
    return b, species_list

#%%
files = ['faune/Madagascar1/madagascar_1.fas','faune/Madagascar1/madagascar_2.fas','faune/Madagascar1/madagascar_3.fas','faune/Madagascar1/madagascar_4.1.fas','faune/Madagascar1/madagascar_4.2.fas','faune/Madagascar1/madagascar_4.3.fas','faune/Madagascar1/madagascar_5.fas','faune/Madagascar1/madagascar_6.fas']
additional_file = 'faune/Madagascar1/madagascar copie.txt'

b, species_list = process_data(files, additional_file)

print(b)
print(species_list)

"""#%%
files_1 = ['faune/Madagascar1/madagascar_1.fas','faune/Madagascar1/madagascar_2.fas','faune/Madagascar1/madagascar_3.fas','faune/Madagascar1/madagascar_4.1.fas','faune/Madagascar1/madagascar_4.2.fas','faune/Madagascar1/madagascar_4.3.fas','faune/Madagascar1/madagascar_5.fas','faune/Madagascar1/madagascar_6.fas']
files_2 = ['faune/Madagascar2/madagascar_2.fas','faune/Madagascar2/madagascar_3.2.fas','faune/Madagascar2/madagascar_3.3.fas','faune/Madagascar2/madagascar_3.fas','faune/Madagascar2/madagascar_4.1.fas','faune/Madagascar2/madagascar_4.2.fas','faune/Madagascar2/madagascar_4.3.fas','faune/Madagascar2/madagascar_5.fas','faune/Madagascar2/madagascar_6.fas']
files_3 = ['faune/Madagascar3/madagascar_2.fas','faune/Madagascar3/madagascar_3.2.fas','faune/Madagascar3/madagascar_3.3.fas','faune/Madagascar3/madagascar_3.4.fas','faune/Madagascar3/madagascar_3.fas','faune/Madagascar3/madagascar_4.1.fas','faune/Madagascar3/madagascar_4.2.fas','faune/Madagascar3/madagascar_5.fas','faune/Madagascar3/madagascar_6.fas']
files_4 = ['faune/Madagascar4/madagascar_3.2.fas','faune/Madagascar4/madagascar_3.3.fas','faune/Madagascar4/madagascar_3.4.fas','faune/Madagascar4/madagascar_3.fas','faune/Madagascar4/madagascar_4.1.fas','faune/Madagascar4/madagascar_4.2.fas','faune/Madagascar4/madagascar_5.fas','faune/Madagascar4/madagascar_6.fas']
files_5 = ['faune/Madagascar5/madagascar_3.2.fas','faune/Madagascar5/madagascar_3.3.fas','faune/Madagascar5/madagascar_3.4.fas','faune/Madagascar5/madagascar_3.5.fas','faune/Madagascar5/madagascar_3.fas','faune/Madagascar5/madagascar_4.1.fas','faune/Madagascar5/madagascar_4.2.fas','faune/Madagascar5/madagascar_6.fas']

species_list_1 = identification.identifyBarcodes(files_1)
species_list_2 = identification.identifyBarcodes(files_2)
species_list_3 = identification.identifyBarcodes(files_3)
species_list_4 = identification.identifyBarcodes(files_4)
species_list_5 = identification.identifyBarcodes(files_5)

b_1 = shannon.biodiversity(species_list_1, ['species'], 1)
b_2 = shannon.biodiversity(species_list_2, ['species'], 1)
b_3 = shannon.biodiversity(species_list_3, ['species'], 1)
b_4 = shannon.biodiversity(species_list_4, ['species'], 1)
b_5 = shannon.biodiversity(species_list_5, ['species'], 1)"""