import requests
import urllib.parse
import io
import webbrowser
import matplotlib.pyplot as plt
import numpy as np
from identification import *
from shannon import *
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from default_vars import *
from win32api import GetSystemMetrics
from tkinter import filedialog
from PIL import Image, ImageTk
w = GetSystemMetrics (0)
h = GetSystemMetrics (1)

class Interface(Frame):
    
    """
    Main window
    All the widgets are stored as attributes of this window
    """
    
    def __init__(self, window, **kwargs):
        Frame.__init__(self, window, width=window_width, height=window_height, bg=window_background_color, **kwargs)
        self.pack() #fill=BOTH
        self.data = {}
        self.widgets = []
        self.fastas = {}
        
        self.form_start_page()
    
    
    def erase_page(self):
        # Destroy previous widgets
        for i in range(len(self.widgets) - 1, -1, -1):
            self.widgets[i].destroy()
            self.widgets.pop()


    def openfile(self):
        filenames = filedialog.askopenfilenames(initialdir = "D:/Arthur/Documents/Python/Lauzhack_2020/biodiversity/faune/",title = "Select file")
        self.filenames = list(filenames)
        self.Lb1 = Listbox(self)
        for i, f in enumerate(self.filenames):
            self.Lb1.insert(i, f)
        self.Lb1.grid(row=4, column=2)
        self.widgets.append(self.Lb1)
        print(self.filenames)
    
    
    def openNewWindow(self, h_window, w_window):
        # Toplevel object which will  be treated as a new window 
        self.newWindow = Toplevel(self)
        # sets the title of the Toplevel widget
        self.newWindow.title("New Window")
        # sets the geometry of toplevel 
        self.newWindow.geometry(f"{w_window}x{h_window}")
        # A Label widget to show in toplevel 
        self.label_new_window = Label(self.newWindow, text ="This is a new window", font=fonts["bold"], fg=font_color, bg=window_background_color)
        self.label_new_window.grid(row=1)
    
    
    def load_im(self, h_window, w_window):
        url = "http://www.boldsystems.org/index.php/TaxBrowser_Maps_CollectionSites?taxid=48327"
        raw_data = urllib.request.urlopen(url).read()
        im = Image.open(io.BytesIO(raw_data))
        ratio = min(im.size[0] / w_window, im.size[1] / h_window)
        im = im.resize((int(im.size[0] / ratio), int(im.size[1] / ratio)))
        image = ImageTk.PhotoImage(im)
        return image
    
    
    def display_im(self):
        h_window, w_window = 700, 700
        image = self.load_im(h_window, w_window)
        self.openNewWindow(h_window, w_window)
        self.label1 = Label(self.newWindow, image=image)
        self.label1.grid(row=2)
        self.newWindow.mainloop()
        
    
    def form_start_page(self):        
        self.erase_page()
        
        # Widgets creation
        self.message = Label(self, text="WELCOME", font=fonts["big bold"], fg=font_color, bg=window_background_color)
        self.message.grid(row=0)
        self.widgets.append(self.message)
        
        # Managing spaces between the big button and the Welcome message
        self.l1 = Label(self, text='             ', bg=window_background_color)
        self.l3 = Label(self, text='             ', bg=window_background_color)
        self.l1.grid(row=1)
        self.l3.grid(row=3)
        self.widgets.append(self.l1)
        self.widgets.append(self.l3)
        
        self.upload_files_button = Button(self, text="To begin, upload your fasta files", command=self.form_page1, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.upload_files_button.grid(row=2)
        self.widgets.append(self.upload_files_button)
        
        self.quit_button = Button(self, text="Quit", command=self.quit, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.quit_button.grid(row=4)
        self.widgets.append(self.quit_button)
    
    
    def stop_before_complete(self):
        resp = messagebox.askyesno("Warning","You will loose all the data you entered.\nDo you want to quit anyway ?", icon='warning')
        if resp:
            self.quit()
    
    
    def cancel_before_complete(self):
        resp = messagebox.askyesno("Warning","You will loose all the data you entered.\nDo you want to come back to step 1 anyway ?", icon='warning')
        if resp:
            self.data = {}
            self.form_start_page()
    
    
    def form_page1(self):
        
        # Destroy previous interface
        self.erase_page()
        self.master.title('Upload your fasta files')
        
        # Create new interface
        self.l0 = Label(self, text='                         ', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l0.grid(row=1, column=0)
        self.widgets.append(self.l0)
        self.l1 = Label(self, text="Year", font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l1.grid(row=2, column=0)
        self.widgets.append(self.l1)
        self.l2 = Label(self, text="Location (lattitude, longitude)", font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l2.grid(row=3, column=0)
        self.widgets.append(self.l2)
        self.l3 = Label(self, text="Fasta files", font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l3.grid(row=4, column=0)
        self.widgets.append(self.l3)
        self.l4 = Label(self, text='                         ', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l4.grid(row=5, column=0)
        self.widgets.append(self.l4)
        self.l5 = Label(self, text='                         ', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l5.grid(row=8, column=0)
        self.widgets.append(self.l5)
        
        self.year = Entry(self, font=fonts["normal"])
        self.year.grid(row=2, column=1)
        self.widgets.append(self.year)
        self.location = Entry(self, font=fonts["normal"])
        self.location.grid(row=3, column=1)
        self.widgets.append(self.location)
        
        self.upload_files_button = Button(self, text="Choose fastas", command=self.openfile, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.upload_files_button.grid(row=4, column=1)
        self.widgets.append(self.upload_files_button)
        
        self.upload_next_year_button = Button(self, text="Confirm selection and upload fasta files for another year", command=self.reload_page1_for_additional_fastas, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.upload_next_year_button.grid(row=6, column=0)
        self.widgets.append(self.upload_next_year_button)
        
        self.skip_button = Button(self, text="Skip (for dev. only)", command=self.skip, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.skip_button.grid(row=7, column=0)
        self.widgets.append(self.skip_button)
        
        self.next_button = Button(self, text="Next", command=self.go_to_page2, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.next_button)
        self.next_button.grid(row=16, column=0)
        self.cancel_button = Button(self, text="Cancel", command=self.cancel_before_complete, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.cancel_button)
        self.cancel_button.grid(row=16, column=1)
        self.quit_button = Button(self, text="Quit", command=self.stop_before_complete, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.quit_button)
        self.quit_button.grid(row=16, column=2)
    
    
    def find_detected_species(self, year):
        index = np.where(np.array(list(self.fastas.keys())) == year)[0]
        self.species = []
        for s in self.identified_species[index][0]:
            if s['taxid'] != 0:
                self.species.append(s['species'])
        self.species = list(set(self.species))
    
    
    def nb_unknown_species(self, year):
        index = np.where(np.array(list(self.fastas.keys())) == year)[0]
        tot = 0
        for s in self.identified_species[index][0]:
            if s['taxid'] == 0:
                tot += 1
        return tot
    
    
    def view_species(self, event):
        year = self.combo_detected_species.get()
        if year != ' - ':
            year = int(year)
            self.find_detected_species(year)
            self.Lb1 = Listbox(self)
            for i, s in enumerate(self.species):
                self.Lb1.insert(i, s)
            self.Lb1.grid(row=1, column=2)
            self.widgets.append(self.Lb1)
            self.Lb1.bind("<<ListboxSelect>>", self.callback)
            
            self.l1_bis.config(text = f'Species ({len(self.species)})')
            self.l2_ter.config(text = self.nb_unknown_species(year))
            self.l3.config(text = f"Cumulative distribution of the species fractions in the environment for year {year}")
            
            self.plot_cumul_dist(year)
            im = Image.open('Cumulative.png')
            image = ImageTk.PhotoImage(im)
            self.label1 = Label(self, image=image)
            self.label1.image = image
            self.label1.grid(row=3)
            self.widgets.append(self.label1)
    
    
    def callback(self, event):
        selection = event.widget.curselection()
        if selection:
            index = selection[0]
            data = event.widget.get(index)
            data = data.replace(' ', '+')
            url = f'https://www.boldsystems.org/index.php/Taxbrowser_Taxonpage?taxon={data}&searchTax=Search+Taxonomy'
            webbrowser.open_new(url)
    
    
    def skip(self):
        
        self.identified_species = np.load('species.npy', allow_pickle=True)
        self.plot_entropy()
        
        # Destroy previous interface
        self.erase_page()
        self.master.title("General informations")
        
        self.l1 = Label(self, text='Year', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l1.grid(row=0, column=1)
        self.widgets.append(self.l1)
        self.l1_bis = Label(self, text='Species (-)', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l1_bis.grid(row=0, column=2)
        self.widgets.append(self.l1_bis)
        self.l1_ter = Label(self, text='Unknown species', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l1_ter.grid(row=0, column=3)
        self.widgets.append(self.l1_ter)
        self.l2 = Label(self, text='Detected species : ', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l2.grid(row=1, column=0)
        self.widgets.append(self.l2)
        self.l2_ter = Label(self, text=' - ', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l2_ter.grid(row=1, column=3)
        self.widgets.append(self.l2_ter)
        self.l3 = Label(self, text="Cumulative distribution of the species fractions in the environment for year - ", font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l3.grid(row=2, column=0)
        self.widgets.append(self.l3)
        
        self.combo_detected_species = ttk.Combobox(self, values=[' - '] + list(self.fastas.keys()))
        self.combo_detected_species.current(0)
        self.combo_detected_species.bind("<<ComboboxSelected>>", self.view_species)
        self.combo_detected_species.grid(row = 1, column=1)
        self.widgets.append(self.combo_detected_species)
        
        self.next_button = Button(self, text="Next", command=self.form_page3, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.next_button)
        self.next_button.grid(row=5, column=0)
        self.cancel_button = Button(self, text="Restart", command=self.cancel_before_complete, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.cancel_button)
        self.cancel_button.grid(row=5, column=1)
        self.quit_button = Button(self, text="Quit", command=self.stop_before_complete, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.quit_button)
        self.quit_button.grid(row=5, column=2)
    
    
    def reload_page1_for_additional_fastas(self):
        
        self.fastas[int(self.year.get())] = self.filenames
        
        # Destroy previous interface
        self.erase_page()
        
        self.form_page1()
    
    
    def plot_cumul_dist(self, year):
        
        plt.figure(figsize=(8, 5))
        self.species = []
        counts = []
        for species in self.identified_species:
            species_temp = []
            for s in species:
                if s['taxid'] != 0:
                    species_temp.append(s['species'])
            counts.append(dict((i, species_temp.count(i)) for i in species_temp))
            self.species += species_temp
            self.species = list(set(self.species))
        
        index = np.where(np.array(list(self.fastas.keys())) == year)[0][0]
        data = np.array(list(counts[index].values())) / np.sum(list(counts[index].values()))
        data = np.sort(data)[::-1]
        for i in range(1, len(data)):
            data[i] += data[i - 1]
        data = list(data)
        data.append(0)
        data = np.array(data)
        data = np.sort(data)
        print(data)
        # plot the cumulative function
        plt.plot(np.arange(len(data)), data, c='darkblue')
        plt.ylim(0, 1)
        plt.xlim(0, len(data) - 1)
        plt.xlabel('Species')
        plt.ylabel('Cumulative fraction')
        plt.savefig('Cumulative.png')
    
    
    def plot_entropy(self):
        
        plt.figure(figsize=(8, 5))
        self.entropies = [biodiversity(species, key_list, 1)['species'] for species in self.identified_species]
        plt.plot(list(self.fastas.keys()), self.entropies, c='darkblue', marker='.')
        for x, y in zip(self.fastas.keys(), self.entropies):
            plt.annotate(str(round(y, 2)), xy=(x, y), textcoords='data')
        plt.xlabel('Year')
        plt.ylabel('Shannon entropy')
        plt.xlim(np.min(list(self.fastas.keys())), np.max(list(self.fastas.keys())))
        plt.savefig('Shanon.png')
    
    
    def go_to_page2(self):
        
        self.fastas[int(self.year.get())] = self.filenames
        self.identified_species = [identifyBarcodes(barcodes) for barcodes in self.fastas.values()]
        
        # Destroy previous interface
        self.erase_page()
        self.master.title("General informations")
        
        self.l1 = Label(self, text='Year', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l1.grid(row=0, column=1)
        self.widgets.append(self.l1)
        self.l1_bis = Label(self, text='Species (-)', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l1_bis.grid(row=0, column=2)
        self.widgets.append(self.l1_bis)
        self.l1_ter = Label(self, text='Unknown species', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l1_ter.grid(row=0, column=3)
        self.widgets.append(self.l1_ter)
        self.l2 = Label(self, text='Detected species : ', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l2.grid(row=1, column=0)
        self.widgets.append(self.l2)
        self.l2_ter = Label(self, text=' - ', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l2_ter.grid(row=1, column=3)
        self.widgets.append(self.l2_ter)
        self.l3 = Label(self, text="Cumulative distribution of the species fractions in the environment for year - ", font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l3.grid(row=2, column=0)
        self.widgets.append(self.l3)
        
        self.combo_detected_species = ttk.Combobox(self, values=[' - '] + list(self.fastas.keys()))
        self.combo_detected_species.current(0)
        self.combo_detected_species.bind("<<ComboboxSelected>>", self.view_species)
        self.combo_detected_species.grid(row = 1, column=1)
        self.widgets.append(self.combo_detected_species)
        
        self.next_button = Button(self, text="Next", command=self.form_page3, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.next_button)
        self.next_button.grid(row=5, column=0)
        self.cancel_button = Button(self, text="Restart", command=self.cancel_before_complete, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.cancel_button)
        self.cancel_button.grid(row=5, column=1)
        self.quit_button = Button(self, text="Quit", command=self.stop_before_complete, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.quit_button)
        self.quit_button.grid(row=5, column=2)
    
    
    def plot_species_evolution(self):
        
        plt.figure(figsize=(8, 5))
        self.species = []
        counts = []
        for species in self.identified_species:
            species_temp = []
            for s in species:
                if s['taxid'] != 0:
                    species_temp.append(s['species'])
            counts.append(dict((i, species_temp.count(i)) for i in species_temp))
            self.species += species_temp
            self.species = list(set(self.species))
        
        counts_through_time = {s : np.array([counts[i][s] if s in counts[i] else 0 for i in range(len(counts))]) for s in self.species}
        number_of_species_through_time = np.array([np.sum([counts_through_time[s][i] for s in self.species]) for i in range(len(list(self.fastas.keys())))])
        # print(number_of_species_through_time)
        # print(counts_through_time)
        
        for s in self.species:
            counts_through_time[s] = counts_through_time[s] / number_of_species_through_time
            plt.plot(list(self.fastas.keys()), counts_through_time[s], label=s)
            plt.xlabel('Year')
            plt.ylabel('Proportion in the environment')
        
        plt.xlim(np.min(list(self.fastas.keys())), np.max(list(self.fastas.keys())))
        plt.legend()
        plt.savefig('Proportions vs time.png')
    
    
    def form_page3(self):
        
        # Destroy previous interface
        self.erase_page()
        self.master.title("General informations")
        
        self.l1 = Label(self, text='Evolution of the most prevalent species during the period of the samples', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l1.grid(row=0, column=0)
        self.widgets.append(self.l1)
        self.l2 = Label(self, text='Shannon entropy of your environment for the years you have selected', font=fonts["normal"], fg=font_color, bg=window_background_color)
        self.l2.grid(row=0, column=1)
        self.widgets.append(self.l2)
        
        self.plot_species_evolution()
        self.plot_entropy()
        
        im = Image.open('Proportions vs time.png')
        image1 = ImageTk.PhotoImage(im)
        self.label1 = Label(self, image=image1)
        self.label1.image = image1
        self.label1.grid(row=1, column=0)
        self.widgets.append(self.label1)
        
        im = Image.open('Shanon.png')
        image2 = ImageTk.PhotoImage(im)
        self.label2 = Label(self, image=image2)
        self.label2.image = image2
        self.label2.grid(row=1, column=1)
        self.widgets.append(self.label2)
        
        self.previous_button = Button(self, text="Previous", command=self.skip, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.previous_button)
        self.previous_button.grid(row=16, column=0)
        self.cancel_button = Button(self, text="Restart", command=self.cancel_before_complete, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.cancel_button)
        self.cancel_button.grid(row=16, column=1)
        self.quit_button = Button(self, text="Quit", command=self.stop_before_complete, font=fonts["normal"], fg=font_color, bg=button_background_color)
        self.widgets.append(self.quit_button)
        self.quit_button.grid(row=16, column=2)