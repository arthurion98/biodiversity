# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 14:28:04 2020

@author: arthu
"""

#import os
#from inspect import getsourcefile
#from os.path import abspath

#path = abspath(getsourcefile(lambda:0))
#path = path[0 : -len('identification.py')]
#os.chdir(path)

import query
import default_vars as parameter

def identifyBarcodes(barcodes):
    """from list of sequence names (FASTA_file) barcodes, convert to list of identified species"""
    identified_species = []
    for barcode in barcodes:
        identified_species.append(identify(barcode))
    
    return identified_species

def identify(sequence_name):
    """from sequence_name (FASTA file), extract the most likely candidate, if none assign 'unidentified' """
    # retrieve identification candidates
    print(sequence_name)
    
    query.FASTAmatch(sequence_name, 'buffer')
    
    similar_species = query.XMLread('buffer.xml')
    
    specie = None
    if len(similar_species) > 0:
        if float(similar_species[0]['similarity']) > parameter._similarity_limit:
            specie = similar_species[0]
    
    if specie != None:
        return query.findTaxonomy(specie['ID'], sequence_name)
    else:
        return {'taxid':0, 'species':sequence_name, 'genus':sequence_name, 'family':sequence_name, 'order':sequence_name, 'class':sequence_name, 'phylum':sequence_name, 'sequence_name':sequence_name}

def addSpecies(list_species, species_to_add_file):
    
    file = open(species_to_add_file, 'r')
    
    lines = file.readlines()
    for line in lines:
        split_line = line.split(', ')
        
        for i in range(int(split_line[6])):
            list_species.append({'taxid':0, 'species':split_line[0], 'genus':split_line[1], 'family':split_line[2], 'order':split_line[3], 'class':split_line[4], 'phylum':split_line[5], 'sequence_name':'none'})
        
    return list_species

"""#%%
files = ['faune/madagascar_1.fas','faune/madagascar_2.fas','faune/madagascar_3.fas']#['jardins/fasta_1.fas','jardins/fasta_2.fas','jardins/fasta_3.fas','jardins/fasta_4.fas','jardins/fasta_5.fas','jardins/fasta_6.fas','jardins/fasta_7.fas','jardins/fasta_8.fas','jardins/fasta_9.fas','jardins/fasta_10.fas','jardins/fasta_11.fas','jardins/fasta_12.fas','jardins/fasta_13.fas','jardins/fasta_14.fas','jardins/fasta_15.fas','jardins/fasta_16.fas','jardins/fasta_17.fas']

print(identifyBarcodes(files))

#%%
query.FASTAmatch('test_sequence.fa', 'testtest')
results = query.XMLread('testtest.xml')
for result in results:
    query.printMatch(result)"""