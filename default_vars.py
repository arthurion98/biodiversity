window_height = 800
window_width = 1600

fonts = {
    "normal" : "Times 16",
    "big" : "Times 32",
    "bold" : "Times 16 bold",
    "italic" : "Times 16, italic",
    "italic bold" : "Times 16 bold italic",
    "bold italic" : "Times 16 bold italic",
    "bold big" : "Times 32 bold",
    "big bold" : "Times 32 bold",
    "italic big" : "Times 32 italic",
    "big italic" : "Times 32 italic",
    "italic bold big" : "Times 32 bol italic",
    "bold italic big" : "Times 32 bold italic",
    "big italic bold" : "Times 32 bold italic",
    "big bold italic" : "Times 32 bold italic"
}

window_background_color = "lightgrey"
button_background_color = "silver"
font_color = "darkblue"

# identification parameters
_similarity_limit = 0.95

# key_list
key_list = ['species'] #,'genus','family','order','class','phylum'