# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 22:36:51 2020

@author: Tâm Nguyên
"""
import requests
import pandas as pd
import xml.dom.minidom

def FASTAmatch(read_name, write_name, species_type = 'COX1_SPECIES'):
    """open FASTA file read_name and search for matches in BOLDSYSTEMS database, save
    resulting matches in a xml file write_name.xml"""
    
    # read FASTA
    file_FASTA = open(read_name, 'r')
    
    lines = file_FASTA.readlines()
    
    cleaned_lines = []
    for line in lines:
        if len(line) > 0:
            if line[0] != '>':
                cleaned_lines.append(line)
    
    sequence = ''.join(cleaned_lines) # more efficient than loop concatenation
    
    # search for match in database
    url = 'http://www.boldsystems.org/index.php/Ids_xml?db={}&sequence={}'.format(species_type, sequence)
    r = requests.get(url, allow_redirects = True)
    
    # write to file
    open('{}.xml'.format(write_name), 'wb').write(r.content)

def XMLread(name):
    """open xml file name and write content to a list of dictionary (match : properties)"""
    
    matches_content = []
    
    # read xml
    doc = xml.dom.minidom.parse(name)

    matches = doc.getElementsByTagName('match')

    for match in matches:
        _id = match.getElementsByTagName('ID')[0].firstChild.nodeValue
        _tax = match.getElementsByTagName('taxonomicidentification')[0].firstChild.nodeValue
        _similarity = match.getElementsByTagName('similarity')[0].firstChild.nodeValue
        
        if match.getElementsByTagName('country')[0].firstChild != None:    
            _country = match.getElementsByTagName('country')[0].firstChild.nodeValue
        else:
            _country = ''
        
        if match.getElementsByTagName('lat')[0].firstChild != None:    
            _lat = match.getElementsByTagName('lat')[0].firstChild.nodeValue
        else:
            _lat = ''
        
        if match.getElementsByTagName('lon')[0].firstChild != None:    
            _lon = match.getElementsByTagName('lon')[0].firstChild.nodeValue
        else:
            _lon = ''
        
        matches_content.append({ 'ID' : _id, 'taxonomicidentification' : _tax, 'similarity' : _similarity, 'country' : _country, 'lat' : _lat, 'lon' : _lon })
        
    return matches_content

def printMatch(match):
    """print a match dictionnary as formalised above"""
    print('{} ({}) : {}'.format(match['taxonomicidentification'], match['ID'], match['similarity']))
    str_buffer = ''
    if len(match['country']) > 0:
        str_buffer = str_buffer.join([match['country']])
        if len(match['lat']) > 0 and len(match['lon']) > 0:
            str_buffer = str_buffer.join([' ', match['lat'], ', ', match['lon']])
    if len(str_buffer) > 0:
        print(str_buffer)

def findTaxonomy(ID, sequence_name):
    
    # retrieve data
    url = 'http://www.boldsystems.org/index.php/API_Public/specimen?ids={}&format=xml'.format(ID)
    r = requests.get(url, allow_redirects = True)
    
    open('buffer.xml', 'wb').write(r.content)
    
    # read xml
    full_doc = xml.dom.minidom.parse('buffer.xml')
    doc = full_doc.getElementsByTagName('taxonomy')[0]
    taxonomy = {'taxid' : doc.getElementsByTagName('species')[0].getElementsByTagName('taxID')[0].firstChild.nodeValue}
    taxonomy['species'] = doc.getElementsByTagName('species')[0].getElementsByTagName('name')[0].firstChild.nodeValue
    taxonomy['genus'] = doc.getElementsByTagName('genus')[0].getElementsByTagName('name')[0].firstChild.nodeValue
    taxonomy['family'] = doc.getElementsByTagName('family')[0].getElementsByTagName('name')[0].firstChild.nodeValue
    taxonomy['order'] = doc.getElementsByTagName('order')[0].getElementsByTagName('name')[0].firstChild.nodeValue
    taxonomy['class'] = doc.getElementsByTagName('class')[0].getElementsByTagName('name')[0].firstChild.nodeValue
    taxonomy['phylum'] = doc.getElementsByTagName('phylum')[0].getElementsByTagName('name')[0].firstChild.nodeValue   
    
    taxonomy['sequence_name'] = sequence_name
    
    return taxonomy

#def findTaxonomy(taxonomicid):
#    """return taxonomic information on the given specie"""
#    # format taxonomicid
#    taxonomicid_separated = taxonomicid.split()
#    new_buffer = []
#    for tax_elem in taxonomicid_separated:
#        new_buffer.append(tax_elem)
#        new_buffer.append('+')
#    new_buffer.pop()
#    new_buffer.append('&dataTypes=basic')
#    
#    taxonomicid_buffer = ''.join(new_buffer)
#    
#    # retrieve data on specie
#    url = 'http://www.boldsystems.org/index.php/API_Tax/TaxonSearch?taxName={}'.format(taxonomicid_buffer)
#    r = requests.get(url, allow_redirects = True)
#    
#    buffer_json = r.content
#    
#    # read json
#    specie_data = pd.read_json(buffer_json)
#    local_taxonomy = specie_data['top_matched_names'][0]
#    
#    #n = specie_data['total_matched_names'][0]
#    #for i in range(n):
#    #    print(specie_data['top_matched_names'][i])
#    
#    taxonomy = {'taxid' : local_taxonomy['taxid'], }
#    
#    return taxonomy
    
        
"""#%%
# testing query capabilities

# request
url_buffer = 'http://www.boldsystems.org/index.php/API_Public/specimen?taxon=Aves&geo=Costa%20Rica&format=tsv'
r = requests.get(url_buffer, allow_redirects = True)

# write to file
open('test.tsv', 'wb').write(r.content)

#%%
# read tsv
test_file_tsv = pd.read_csv('test.tsv', sep='\t')

#print(test_file_tsv.keys())
print(test_file_tsv['processid'].values)

#%%
# test match

FASTAmatch('test_sequence.fa', 'test_match')

#%%
# read xml
doc = xml.dom.minidom.parse('test_match.xml')

matches = doc.getElementsByTagName('match')
print(matches.length)

for match in matches:
    print('({}) {} : {}'.format(match.getElementsByTagName('ID')[0].firstChild.nodeValue, match.getElementsByTagName('taxonomicidentification')[0].firstChild.nodeValue, match.getElementsByTagName('similarity')[0].firstChild.nodeValue))
    
#%%
# test read xml
results = XMLread('test_match.xml')
for result in results:
    printMatch(result)
    
#%%
# test data specie
#findTaxonomy('Anaspides tasmaniae')
findTaxonomy('GBCM0002-06')"""