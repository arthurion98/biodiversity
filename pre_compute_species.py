from identification import *
from shannon import *

files_1 = ['faune/Madagascar1/madagascar_1.fas','faune/Madagascar1/madagascar_2.fas','faune/Madagascar1/madagascar_3.fas','faune/Madagascar1/madagascar_4.1.fas','faune/Madagascar1/madagascar_4.2.fas','faune/Madagascar1/madagascar_4.3.fas','faune/Madagascar1/madagascar_5.fas','faune/Madagascar1/madagascar_6.fas']
files_2 = ['faune/Madagascar2/madagascar_2.fas','faune/Madagascar2/madagascar_3.2.fas','faune/Madagascar2/madagascar_3.3.fas','faune/Madagascar2/madagascar_3.fas','faune/Madagascar2/madagascar_4.1.fas','faune/Madagascar2/madagascar_4.2.fas','faune/Madagascar2/madagascar_4.3.fas','faune/Madagascar2/madagascar_5.fas','faune/Madagascar2/madagascar_6.fas']
files_3 = ['faune/Madagascar3/madagascar_2.fas','faune/Madagascar3/madagascar_3.2.fas','faune/Madagascar3/madagascar_3.3.fas','faune/Madagascar3/madagascar_3.4.fas','faune/Madagascar3/madagascar_3.fas','faune/Madagascar3/madagascar_4.1.fas','faune/Madagascar3/madagascar_4.2.fas','faune/Madagascar3/madagascar_5.fas','faune/Madagascar3/madagascar_6.fas']
files_4 = ['faune/Madagascar4/madagascar_3.2.fas','faune/Madagascar4/madagascar_3.3.fas','faune/Madagascar4/madagascar_3.4.fas','faune/Madagascar4/madagascar_3.fas','faune/Madagascar4/madagascar_4.1.fas','faune/Madagascar4/madagascar_4.2.fas','faune/Madagascar4/madagascar_5.fas','faune/Madagascar4/madagascar_6.fas']
files_5 = ['faune/Madagascar5/madagascar_3.2.fas','faune/Madagascar5/madagascar_3.3.fas','faune/Madagascar5/madagascar_3.4.fas','faune/Madagascar5/madagascar_3.5.fas','faune/Madagascar5/madagascar_3.fas','faune/Madagascar5/madagascar_4.1.fas','faune/Madagascar5/madagascar_4.2.fas','faune/Madagascar5/madagascar_6.fas']

species_list_1 = identifyBarcodes(files_1)
species_list_2 = identifyBarcodes(files_2)
species_list_3 = identifyBarcodes(files_3)
species_list_4 = identifyBarcodes(files_4)
species_list_5 = identifyBarcodes(files_5)

b_1 = biodiversity(species_list_1, ['species'], 1)
b_2 = biodiversity(species_list_2, ['species'], 1)
b_3 = biodiversity(species_list_3, ['species'], 1)
b_4 = biodiversity(species_list_4, ['species'], 1)
b_5 = biodiversity(species_list_5, ['species'], 1)

np.save('species.npy', [species_list_1, species_list_2, species_list_3, species_list_4, species_list_5])