# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 22:36:51 2020

@author: Tâm Nguyên
"""
import requests
import pandas as pd
import xml.dom.minidom

def FASTAmatch(read_name, write_name, species_type = 'COX1_SPECIES_PUBLIC'):
    """open FASTA file read_name and search for matches in BOLDSYSTEMS database, save
    resulting matches in a xml file write_name.xml"""
    
    # read FASTA
    file_FASTA = open(read_name, 'r')
    
    lines = file_FASTA.readlines()
    sequence = ''.join(lines) # more efficient than loop concatenation
    
    # search for match in database
    url = 'http://www.boldsystems.org/index.php/Ids_xml?db={}&sequence={}'.format(species_type, sequence)
    r = requests.get(url, allow_redirects = True)
    
    # write to file
    open('{}.xml'.format(write_name), 'wb').write(r.content)

def XMLread(name):
    """open xml file name and write content to a list of dictionary (match : properties)"""
    
    matches_content = []
    
    # read xml
    doc = xml.dom.minidom.parse('test_match.xml')

    matches = doc.getElementsByTagName('match')

    for match in matches:
        _id = match.getElementsByTagName('ID')[0].firstChild.nodeValue
        _tax = match.getElementsByTagName('taxonomicidentification')[0].firstChild.nodeValue
        _similarity = match.getElementsByTagName('similarity')[0].firstChild.nodeValue
        
        if match.getElementsByTagName('country')[0].firstChild != None:    
            _country = match.getElementsByTagName('country')[0].firstChild.nodeValue
        else:
            _country = ''
        
        if match.getElementsByTagName('lat')[0].firstChild != None:    
            _lat = match.getElementsByTagName('lat')[0].firstChild.nodeValue
        else:
            _lat = ''
        
        if match.getElementsByTagName('lon')[0].firstChild != None:    
            _lon = match.getElementsByTagName('lon')[0].firstChild.nodeValue
        else:
            _lon = ''
        
        matches_content.append({ 'ID' : _id, 'taxonomicidentification' : _tax, 'similarity' : _similarity, 'country' : _country, 'lat' : _lat, 'lon' : _lon })
        
    return matches_content

def printMatch(match):
    """print a match dictionnary as formalised above"""
    print('{} ({}) : {}'.format(match['taxonomicidentification'], match['ID'], match['similarity']))
    str_buffer = ''
    if len(match['country']) > 0:
        str_buffer.join([match['country']])
        if len(match['lat']) > 0 and len(match['lon']) > 0:
            str_buffer.join([' ', match['lat'], ', ', match['lon']])
    if len(str_buffer) > 0:
        print(str_buffer)